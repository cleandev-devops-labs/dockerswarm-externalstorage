# Docker Swarm - External Storage (NFS)
-----

## Entorno
- SO: Ubuntu 20.04

## Requisitos  
- Instalacion de Virtualbox
- Instalacion de Vagrant

## Informacion maquina virtuales
-----
### NfsServer (nfsServer)

| Maquina Virtual | nfsServer (nfs-server)                                              |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.20                                                        |
| **ROLE**        | **Servidor Nfs**                                                    |

### Manager01 (manager01)
| Maquina Virtual | manager01 (dsw-manager-01)                                          |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.30                                                        |
| **ROLE**        | **Nodo docker swarm maestro**                                       |

### Manager02 (manager02)
| Maquina Virtual | manager02 (dsw-manager-02)                                          |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.40                                                        |
| **ROLE**        | **Nodo docker swarm maestro**                                       |

### Manager03 (manager03)
| Maquina Virtual | manager03 (dsw-manager-03)                                          |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.50                                                        |
| **ROLE**        | **Nodo docker swarm maestro**                                       | 

### Worker01 (worker01)
| Maquina Virtual | worker01 (dsw-worker-01)                                            |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.60                                                        |
| **ROLE**        | **Nodo docker swarm esclavo**                                       |

### Worker02 (worker02)
| Maquina Virtual | worker01 (dsw-worker-02)                                            |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.70                                                        |
| **ROLE**        | **Nodo docker swarm esclavo**                                       | 

## Step 1: Verificacion de requisitos  
------

```
$ virtualbox --help
```
```
Oracle VM VirtualBox VM Selector v6.1.16
(C) 2005-2020 Oracle Corporation
All rights reserved.

No special options.

If you are looking for --startvm and related options, you need to use VirtualBoxVM.
```

```
$ vagrant --version
```

```
Vagrant 2.2.9
```

## Shortcuts  
#### Maquinas Vagrant
```
$ vagrant global-status
```

#### Eliminar VM
``` 
$ vagrant destroy [nfsServer|nfsClient]
```
#### Iniciar VMs

**Nota**: En el directorio donde raiz del proyecto
```
$ vagrant up
```

#### Reiniciar el servidor nfs
``` 
$ sudo systemctl restart nfs-server
```

#### Crear un volumen de docker mediante nfs 
```
$ docker volume create --driver local --opt type=nfs --opt o=addr=xxx.xxx.xx.xx,nfsvers=4 --opt device=:/xx/xx namevolume
```

#### Crear un servicio de docker von un volume tipo nfs
```
$ docker service create --mount 'type=volume,src=nfsexample,dst=/xxx,volume-driver=local,volume-opt=type=nfs,volume-opt=device=:/xx/xx/,"volume-opt=o=addr=xxx.xxx.xx.xx,nfsvers=4"' --name storage httpd
```

## Links
https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/  

